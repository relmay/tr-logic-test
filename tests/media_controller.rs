mod common;

use rocket::http::ContentType;

use std::path::PathBuf;

use std::fs;

pub fn get_test_resource_path(filename: &'static str) -> PathBuf {
    let mut cargo_root_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    cargo_root_path.push("tests/resources");
    cargo_root_path.push(filename);

    cargo_root_path
}

fn read_test_json(filename: &'static str) -> String {
    fs::read_to_string(get_test_resource_path(filename)).expect("Failed to read test JSON!")
}

#[test]
fn should_upload_one_valid_base64_image() {
    let client = common::get_client();
    let body = read_test_json("upload_one_valid_base64_image.json");
    let mut response = client.post("/")
        .header(ContentType::JSON)
        .body(body)
        .dispatch();
    assert!(response.body_string().expect("Body is None!").contains("0"));
}

#[test]
fn shouldnt_upload_invalid_base64_image() {
    let client = common::get_client();
    let body = read_test_json("upload_invalid_base64_image.json");
    let mut response = client.post("/")
        .header(ContentType::JSON)
        .body(body)
        .dispatch();
    assert!(response.body_string().expect("Body is None!").contains("2"));
}

#[test]
fn shouldnt_upload_invalid_image_mime() {
    let client = common::get_client();
    let body = read_test_json("upload_invalid_image_mime.json");
    let mut response = client.post("/")
        .header(ContentType::JSON)
        .body(body)
        .dispatch();
    assert!(response.body_string().expect("Body is None!").contains("4"));    
}

#[test]
fn should_upload_image_from_url() {
    let client = common::get_client();
    let body = r#"{ "url": "https://www.mozilla.org/media/img/logos/firefox/logo-quantum-wordmark-white.bd1944395fb6.png" }"#;
    let mut response = client.post("/")
        .header(ContentType::JSON)
        .body(body)
        .dispatch();

    assert!(response.body_string().unwrap().contains("0"));
}

#[test]
fn shouldnt_upload_wrong_file() {
    let client = common::get_client();
    let body = r#"{ "url": "https://www.mozilla.org" }"#;
    let mut response = client.post("/")
        .header(ContentType::JSON)
        .body(body)
        .dispatch();
    
    assert!(response.body_string().unwrap().contains("4"));
}

// #[test]
// fn shouldnt_upload_image_bigger_than_size_limit() {
//     let client = common::get_client();
//     let body = r#"{ "url": "http://komotoz.ru/photo/porody_sobak/photos/sibirskij_haski/sibirskij_haski_01.jpg" }"#;
//     let mut response = client.post("/")
//         .header(ContentType::JSON)
//         .body(body)
//         .dispatch();

//     assert!(response.body_string().unwrap().contains("6"));
// }