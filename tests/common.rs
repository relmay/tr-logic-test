extern crate tr_logic_test;

use rocket::local::Client;

pub fn get_client() -> Client {
    Client::new(tr_logic_test::ignite()).expect("Invelid Client!")
}