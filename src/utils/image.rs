use std::fs::OpenOptions;
use std::io::Write;

use std::io;

use multipart::server::save::Entries;
use multipart::server::save::SavedData;

use std::fs::File;
use std::io::Read;

use uuid::Uuid;

use image::ImageError;

use crate::config;

#[derive(Debug)]
pub enum Error {
    Base64Error(base64::DecodeError),
    IoError(std::io::Error),
    ReqwestError(reqwest::Error),
    ImageError(ImageError),
    SizeLimit,
    NotImageFile,
    Unknow,
}

impl From<base64::DecodeError> for Error {
    fn from(from: base64::DecodeError) -> Error {
        Error::Base64Error(from)
    }
}

impl From<std::io::Error> for Error {
    fn from(from: std::io::Error) -> Error {
        Error::IoError(from)
    }
}

impl From<reqwest::Error> for Error {
    fn from(from: reqwest::Error) -> Error {
        Error::ReqwestError(from)
    }
}

impl From<ImageError> for Error {
    fn from(from: ImageError) -> Error {
        Error::ImageError(from)
    }
}

pub fn save_from_base64(image_base64: String, mime_type: String) -> Result<(), Error> {
    if !is_mime_type_valid(&mime_type) {
        return Err(Error::NotImageFile);
    }

    let image = base64::decode(&image_base64)?;
    save_image(&image, &mime_type)?;

    Ok(())
}

fn is_mime_type_valid<'a>(mime: &'a str) -> bool {
    match mime {
        "image/jpeg" => true,
        "image/png" => true,
        "image/svg+xml" => true,
        "image/gif" => true,
        _ => false
    }
}

fn get_extension_from_mime<'a>(mime: &'a str) -> &'a str {
    match mime {
        "image/jpeg" => "jpg",
        "image/png" => "png",
        "image/svg+xml" => "svg",
        "image/gif" => "gif",
        _ => panic!("Попытка получить расширение файла для недопустимого MIME type!")
    }
}

pub fn save_from_url(url: String) -> Result<(), Error> {
    let mut response = reqwest::get(&url)?;
    let mut image: Vec<u8> = Vec::new();
    io::copy(&mut response, &mut image)?;
    
    let response_content_type = response.headers().get("content-type");

    if let Some(response_content_type) = response_content_type {
        let response_content_type = response_content_type.to_str().expect("Response Header .to_str failed!");
        if !is_mime_type_valid(response_content_type) {
            return Err(Error::NotImageFile);
        }
        if response.content_length().unwrap() > config::IMAGE_SIZE_LIMIT {
            return Err(Error::SizeLimit);
        }

        save_image(&image, response_content_type)?;

        Ok(())        
    } else {
        return Err(Error::NotImageFile)
    }
}

fn get_saving_path<'a>(file_type: &'a str, uuid: &String) -> String {
    format!("{}/{}.{}", config::IMAGE_PATH, uuid, file_type)
}

fn get_thumb_saving_path(uuid: &String) -> String {
    format!("{}/{}-thumb.jpg", config::THUMB_IMAGE_PATH, uuid)

}

fn generate_thumbnail(image: &Vec<u8>, uuid: &String) -> Result<(), ImageError> {
    image::load_from_memory(&image)?
        .thumbnail(config::THUMBNAIL_WIDTH, config::THUMBNAIL_HEIGHT)
        .save(get_thumb_saving_path(uuid))?;
    
    Ok(())
}

fn save_image<'a>(image: &Vec<u8>, mime: &'a str) -> Result<(), Error> {
    let file_extension = get_extension_from_mime(mime);

    let uuid = Uuid::new_v4();
    let path = get_saving_path(file_extension, &uuid.to_string());

    generate_thumbnail(image, &uuid.to_string())?;

    let mut file = OpenOptions::new().write(true).create_new(true).open(path)?;
    file.write_all(image)?;

    Ok(())
}

pub fn save_images_from_multipart(entries: Entries) -> Result<(), Error> {
    for field in entries.fields {
        for f in field.1 {
            let field_content_type = f.headers.content_type.clone().unwrap().to_string();
            let field_content_type = field_content_type.as_str();
            if !is_mime_type_valid(field_content_type) {
                return Err(Error::NotImageFile);
            }
            match f.data {
                SavedData::Bytes(b) => save_image(&b, field_content_type)?,
                SavedData::File(f, _h) => {
                    let mut file = File::open(f)?;
                    let mut buffer = Vec::new();
                    file.read_to_end(&mut buffer)?;
                    save_image(&buffer, field_content_type)?;
                },
                _ => return Err(Error::NotImageFile)
            };
        }
    };

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    pub fn shouldnt_upload_image_bigger_than_size_limit() {
        let result = save_from_url("http://komotoz.ru/photo/porody_sobak/photos/sibirskij_haski/sibirskij_haski_01.jpg".to_string());
        
        match result {
            Ok(_) => assert!(false),
            Err(e) => match e {
                Error::SizeLimit => assert!(true),
                _ => assert!(false)
            }
        }
    }
}