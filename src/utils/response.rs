use rocket_contrib::json::Json;

#[derive(Serialize)]
pub struct Response {
    code: i32,
    message: String
}

pub enum ErrorStatus {
    Unknow = 1,
    Base64 = 2,
    BadUrl = 3,
    NotImageFile = 4,
    InvalidMutipartFormData = 5,
    SizeLimit = 6,
    IncorrectRequest = 7,
}

impl ToString for ErrorStatus {
    fn to_string(&self) -> String {
        match self {
            ErrorStatus::Unknow => "Неизвестная ошибка!",
            ErrorStatus::Base64 => "Отправлен некорректный BASE64",
            ErrorStatus::BadUrl => "Указан неверный URL!",
            ErrorStatus::NotImageFile => "Отправлено не изображение!",
            ErrorStatus::InvalidMutipartFormData => "Вы отправили некорректный multipart-formdata!",
            ErrorStatus::SizeLimit => "Размер изображения слишком большой!",
            ErrorStatus::IncorrectRequest => "Некорректный запрос!"
        }.to_string()
    }
}

impl Response {
    pub fn get_success() -> Json<Self> {
        Json(Response {
            code: 0,
            message: "Ok".to_string()
        })
    }

    pub fn get_error(error: ErrorStatus) -> Json<Self> {
        Json(Response {
            message: error.to_string(),
            code: error as i32,
        })
    }
}