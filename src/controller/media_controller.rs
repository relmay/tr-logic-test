use rocket::Route;
use rocket_contrib::json::Json;

use multipart::server::Multipart;
use multipart::server::save::SaveResult::*;

use rocket::Data;
use rocket::http::ContentType;

use crate::utils::response::{Response, ErrorStatus};
use crate::utils::image::{self, Error as ImageError};
use crate::config;

#[derive(Deserialize)]
struct ImageEntity {
    mime: String,
    image_base64: String,
}

#[derive(Deserialize)]
struct UploadImageByJsonRequest {
    images: Option<Vec<ImageEntity>>,
    url: Option<String>,
}

#[post("/", data = "<data>", format = "application/json")]
fn upload_images_by_json(
    data: Json<UploadImageByJsonRequest>,
) -> Json<Response> {
    if data.images.is_some() {
        return upload_base64_images(data.0.images.unwrap());
    } else if data.url.is_some() {
        return upload_from_url(data.0.url.unwrap());
    } else {
        return Response::get_error(ErrorStatus::IncorrectRequest);
    }
}

fn upload_base64_images(
    images: Vec<ImageEntity>,
) -> Json<Response> {
    for image in images {
        match image::save_from_base64(image.image_base64, image.mime) {
            Ok(_) => (),
            Err(e) => match e {
                ImageError::Base64Error(_) => return Response::get_error(ErrorStatus::Base64),

                ImageError::NotImageFile => return Response::get_error(ErrorStatus::NotImageFile),

                ImageError::IoError(e) => {
                    println!("[ERROR]: {:?}", e);
                    return Response::get_error(ErrorStatus::Unknow)
                },

                _ => {
                    println!("[ERROR]: {:?}", e);
                    return Response::get_error(ErrorStatus::Unknow)                    
                }
            } 
        };
    }

    Response::get_success()
}

fn upload_from_url(
    url: String,
) -> Json<Response> {
    match image::save_from_url(url) {
        Ok(_) => (),
        Err(e) => match e {
            ImageError::ReqwestError(e) => {
                println!("[ERROR]: {:?}", e);
                return Response::get_error(ErrorStatus::BadUrl);
            },

            ImageError::NotImageFile => {
                return Response::get_error(ErrorStatus::NotImageFile);
            }

            ImageError::SizeLimit => {
                return Response::get_error(ErrorStatus::SizeLimit);
            }
            _ => {
                println!("[ERROR]: {:?}", e);
                return Response::get_error(ErrorStatus::Unknow)
            }
        }
    };

    Response::get_success()
}

#[post("/", data = "<data>", rank = 2)]
fn upload_multipart(
    data: Data,
    content_type: &ContentType,
) -> Json<Response> {
    if !content_type.is_form_data() {
        return Response::get_error(ErrorStatus::InvalidMutipartFormData);
    }

    let boundary = match content_type.params().find(|&(k, _)| k =="boundary") {
        Some((_, boundary)) => boundary,
        None => return Response::get_error(ErrorStatus::InvalidMutipartFormData)
    };

    match Multipart::with_body(data.open(), boundary).save().size_limit(config::IMAGE_SIZE_LIMIT).temp() {
        Full(entries) => {
            match image::save_images_from_multipart(entries) {
                Ok(_) => (),
                Err(e) => match e {
                    ImageError::NotImageFile => return Response::get_error(ErrorStatus::NotImageFile),
                    _ => {
                        println!("[ERROR]: {:?}", e);
                        return Response::get_error(ErrorStatus::Unknow);
                    }
                } 
            };
        }
        _ => return Response::get_error(ErrorStatus::InvalidMutipartFormData)
    };

    Response::get_success()
}

pub fn get_routes() -> Vec<Route> {
    routes![upload_images_by_json, upload_multipart]
}
