#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket_codegen;
#[macro_use]
extern crate serde_derive;

pub mod controller;
pub mod utils;
pub mod config;

use rocket::Rocket;

pub fn ignite() -> Rocket {
    rocket::ignite().mount("/", controller::media_controller::get_routes())
}
