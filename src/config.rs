pub const IMAGE_PATH: &str = "/var/lib/app/files";
pub const THUMB_IMAGE_PATH: &str = "/var/lib/app/files/thumbs";

pub const THUMBNAIL_WIDTH: u32 = 100;
pub const THUMBNAIL_HEIGHT: u32 = 100;

// Для смены лимитов по base64 нужно поменять значение
// [global.limits] json на нужное.
#[cfg(not(test))]
pub const IMAGE_SIZE_LIMIT: u64 = 10000000; // 10MB
#[cfg(test)]
pub const IMAGE_SIZE_LIMIT: u64 = 100000; // 100KB